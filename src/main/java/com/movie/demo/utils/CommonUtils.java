package com.movie.demo.utils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CommonUtils {

	public static LocalDateTime parseStringToDate(String reg_date) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return LocalDateTime.parse(reg_date, dtf);
	}

	public static boolean findDateDifference(LocalDateTime reg_date, LocalDateTime now) {
		Duration d1 = Duration.between(reg_date, now);
		Duration d2 = Duration.ofHours(24);
		if (d1.compareTo(d2) > 0) {
		   return true;
		} else {
		    return false;
		}
		
	}

	public static String parseDateToString(LocalDateTime reg_date) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return dtf.format(reg_date);

	}

}
