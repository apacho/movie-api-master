package com.movie.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.movie.demo.entity.UserMovie;

public interface UserMovieRepository extends JpaRepository<UserMovie, Long> {

	@Query("SELECT COUNT(u) FROM UserMovie u WHERE u.userId =:userId  AND u.movieId =:movieId")
	Long countRow(@Param("userId") long userId, @Param("movieId") long movieId);

}
