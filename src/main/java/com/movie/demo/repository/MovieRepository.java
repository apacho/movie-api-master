package com.movie.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.movie.demo.entity.Movie;
import com.movie.demo.entity.UserMovie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

	@Query("select c from UserMovie c where c.userId = :userId")
	public List<UserMovie> findMoviesByUserId(@Param("userId") long userId);

	@Query("select m from Movie m where m.movieTitle = :movieTitleOrGenre OR m.movieGenre =:movieTitleOrGenre")
	public Optional<Movie> findMovieByTitleAndGenre(@Param("movieTitleOrGenre") String movieTitleOrGenre);

}
