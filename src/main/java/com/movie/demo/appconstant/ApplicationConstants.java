package com.movie.demo.appconstant;

public class ApplicationConstants {

	private ApplicationConstants() {

	}

	public static final Integer LOGIN_SUCCESSFULL_CODE = 9001;
	public static final String LOGIN_SUCCESSFULL = "Your logined successfully";

	public static final Integer LOGIN_FAILURE_CODE = 9004;
	public static final String LOGIN_FAILURE = "please enter valid credentails";

	public static final Integer REGISTER_SUCCESS_CODE = 9006;
	public static final String REGISTER_SUCCESS = "Congrajulations! User Registered Successfully";

	public static final Integer REGISTER_FAILURE_CODE = 9008;
	public static final String REGISTER_FAILURE = "Please register another mail address !mail adress is exits already";

}
