package com.movie.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MovieDto {

	private Long movieId;
	private String movieTitle;
	private String movieGenre;
	private String movieDesc;
	private String watchMovie;

}
