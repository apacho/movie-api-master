package com.movie.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginResponseDto {

	private String message;
	private Integer code;
	private String user_status;
	
}
