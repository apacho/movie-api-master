package com.movie.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RegisterDto {
	private String userName;
	private String emailId;
	private String password;
	private String userStatus;

}
