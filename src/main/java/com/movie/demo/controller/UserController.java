package com.movie.demo.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.movie.demo.dto.LoginDto;
import com.movie.demo.dto.LoginResponseDto;
import com.movie.demo.dto.RegisterDto;
import com.movie.demo.dto.ResponseDto;
import com.movie.demo.entity.Movie;
import com.movie.demo.entity.UserMovie;
import com.movie.demo.service.MovieService;
import com.movie.demo.service.UserService;

@RestController
public class UserController {
	private final static Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserService userService;
	@Autowired
	MovieService movieService;

	@PostMapping("/users")
	public ResponseDto userRegister(@RequestBody RegisterDto registerDto) {
		return userService.register(registerDto);

	}

	@PostMapping("/users/login")
	public ResponseEntity<LoginResponseDto> login(@RequestBody LoginDto loginDto) {
		logger.info("/users/login  ::", loginDto.getEmailId());
		return new ResponseEntity<LoginResponseDto>(userService.validate(loginDto), new HttpHeaders(), HttpStatus.OK);
		// return ResponseEntity.status(HttpStatus.OK).body("" +
		// userService.validate(loginDto));

	}

	@GetMapping("/users/{userId}/movies")
	public ResponseEntity<List<Movie>> listMoviesByUserId(@PathVariable long userId) {
		logger.info("listMoviesByUserIds");
		List<UserMovie> userMovieList = movieService.findMoviesByUserId(userId);
		List<Long> temp = new ArrayList<Long>();
		Optional<List<Movie>> moviesList = null;
		userMovieList.forEach(movie -> {
			if (!Objects.isNull(movie.getMovieId())) {
				temp.add(movie.getMovieId());
			}

		});
		logger.info("=================" + temp);
		moviesList = movieService.findMoviesByUserId(temp);

		return new ResponseEntity<List<Movie>>(moviesList.isPresent() ? moviesList.get() : Collections.EMPTY_LIST,
				new HttpHeaders(), HttpStatus.OK);
	}

}
