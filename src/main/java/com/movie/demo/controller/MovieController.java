package com.movie.demo.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.movie.demo.dto.MovieDto;
import com.movie.demo.entity.Movie;
import com.movie.demo.entity.User;
import com.movie.demo.entity.UserMovie;
import com.movie.demo.exception.InvalidMovie;
import com.movie.demo.exception.InvalidUser;
import com.movie.demo.service.MovieService;
import com.movie.demo.service.UserMovieService;
import com.movie.demo.service.UserService;

@RestController
public class MovieController {
	private final static Logger logger = LoggerFactory.getLogger(MovieController.class);

	@Autowired
	MovieService movieService;
	@Autowired
	UserService userService;
	@Autowired
	UserMovieService userMovieService;

	@GetMapping("/movies")
	public List<MovieDto> getAllMovies() {
		return movieService.getAllMovieInfo();

	}

	@PostMapping("/movies/movie")
	public ResponseEntity<MovieDto> saveMovieByUserId(@RequestParam("movieTitleOrGenre") String movieTitleOrGenre,
			@RequestParam("userId") long userId) {
		logger.info("Finding Movie " + movieTitleOrGenre + "  for User" + userId);
		Optional<Movie> movie = movieService.findMovieByTitleAndGenre(movieTitleOrGenre);
		MovieDto movieDto = new MovieDto();
		Optional<User> user = userService.findByUserId(userId);
				
		if(movie.isPresent() && user.isPresent()) {
			Movie temp = movie.get();
			long count = userMovieService.countRow(userId, temp.getMovieId());
			movieDto.setMovieDesc(temp.getMovieDesc());
			movieDto.setMovieGenre(temp.getMovieGenre());
			movieDto.setMovieTitle(temp.getMovieTitle());
			movieDto.setMovieId(temp.getMovieId());
			if(count == 0) {
				UserMovie userMovie = new UserMovie();
				userMovie.setMovieId(movie.get().getMovieId());
				userMovie.setUserId(userId);
				userMovieService.save(userMovie);
				movieDto.setWatchMovie("Movie is added in your watchlist");
			}else {
				movieDto.setWatchMovie("Movie is already added in your watchlist");
			}
		} else if(!user.isPresent()) {
			throw new InvalidUser("You are not registred user!");
		}else if(!movie.isPresent()){
			throw new InvalidMovie("We don't have movie by given movie title or description," +movieTitleOrGenre+ " please search different!");
		}
		
		return new ResponseEntity<MovieDto>(movieDto, new HttpHeaders(), HttpStatus.OK);

	}
	
}
