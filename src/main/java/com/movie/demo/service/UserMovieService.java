package com.movie.demo.service;

import org.springframework.stereotype.Service;

import com.movie.demo.entity.UserMovie;

@Service
public interface UserMovieService {

	public UserMovie save(UserMovie userMovie);
	
	public long countRow(long userId, long movieId);
}
