package com.movie.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.movie.demo.dto.MovieDto;
import com.movie.demo.entity.Movie;
import com.movie.demo.entity.User;
import com.movie.demo.entity.UserMovie;

@Service
public interface MovieService {

	public List<MovieDto> getAllMovieInfo();

	public MovieDto getWatchMovieData(Long movieId);
	
	public List<UserMovie> findMoviesByUserId(long userId);
	
	public Optional<List<Movie>> findMoviesByUserId(List movieList);
	
	public Optional<Movie> findMovieByTitleAndGenre(String movieTitleOrDesc);

}
