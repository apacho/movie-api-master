package com.movie.demo.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.movie.demo.dto.LoginDto;
import com.movie.demo.dto.LoginResponseDto;
import com.movie.demo.dto.RegisterDto;
import com.movie.demo.dto.ResponseDto;
import com.movie.demo.entity.User;

@Service
public interface UserService {

	public ResponseDto register(RegisterDto registerDto);
	public LoginResponseDto validate(LoginDto loginDto);
	
	public Optional<User> findByUserId(long userId);
}
