package com.movie.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "movie")
@Getter
@Setter
@Data
@ToString
public class Movie {

	@Id
	@Column(name = "movie_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long movieId;
	@Column(name = "movie_title")
	private String movieTitle;
	@Column(name = "movie_genre")
	private String movieGenre;
	@Column(name = "movie_desc")
	private String movieDesc;

}
