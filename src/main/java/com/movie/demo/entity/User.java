package com.movie.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Data
@ToString
@Table(name = "users")
public class User {

	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	@Column(name = "user_name")
	private String ename;
	@Column(name = "email", unique = true, length=100)
	private String email;
	@Column(name = "password")
	private String password;
	@Column(name = "user_status")
	private String userStatus;
	@Column(name = "time_stamp")
	private String registration_time;

}
