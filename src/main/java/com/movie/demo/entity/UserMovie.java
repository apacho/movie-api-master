package com.movie.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "user_movie")
@Getter
@Setter
@Data
@ToString
public class UserMovie {

	@Id
	@Column(name = "user_movie_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userMovieId;

	private Long userId;
	private Long movieId;
	
	public UserMovie(Long userMovieId, Long userId, Long movieId) {
		super();
		this.userMovieId = userMovieId;
		this.userId = userId;
		this.movieId = movieId;
	}
	
	public UserMovie() {
		super();
	}
	
	
	

}
