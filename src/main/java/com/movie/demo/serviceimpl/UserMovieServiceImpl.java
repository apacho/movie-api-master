package com.movie.demo.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.demo.entity.UserMovie;
import com.movie.demo.repository.UserMovieRepository;
import com.movie.demo.service.UserMovieService;

@Service
public class UserMovieServiceImpl implements UserMovieService{

	@Autowired
	UserMovieRepository userMovieRepo;

	@Override
	public UserMovie save(UserMovie userMovie) {
		return userMovieRepo.save(userMovie );
	}
	
	@Override
	public long countRow(long userId, long movieId) {
		return userMovieRepo.countRow(userId, movieId);
	}

}
