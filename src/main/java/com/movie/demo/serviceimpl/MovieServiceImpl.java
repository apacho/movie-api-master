package com.movie.demo.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movie.demo.dto.MovieDto;
import com.movie.demo.entity.Movie;
import com.movie.demo.entity.UserMovie;
import com.movie.demo.repository.MovieRepository;
import com.movie.demo.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	MovieRepository movieRepo;

	ModelMapper mapper = new ModelMapper();

	@Override
	public List<MovieDto> getAllMovieInfo() {

		List<Movie> movies = movieRepo.findAll();

		List<MovieDto> listOfMovies = new ArrayList<MovieDto>();

		for (Movie movieInfo : movies) {
			MovieDto movieDto = mapper.map(movieInfo, MovieDto.class);

			listOfMovies.add(movieDto);
		}

		return listOfMovies;
	}

	@Override
	public MovieDto getWatchMovieData(Long movieId) {

		Optional<Movie> movieData = movieRepo.findById(movieId);

		MovieDto movieDto = new MovieDto();

		movieDto.setMovieDesc(movieData.get().getMovieDesc());
		movieDto.setMovieId(movieData.get().getMovieId());
		movieDto.setMovieTitle(movieData.get().getMovieTitle());
		movieDto.setMovieGenre(movieData.get().getMovieGenre());
		movieDto.setWatchMovie("Please watch and enjoy the movie");

		return movieDto;
	}

	@Override
	public List<UserMovie> findMoviesByUserId(long userId) {
		return movieRepo.findMoviesByUserId(userId);
	}

	@Override
	public Optional<List<Movie>> findMoviesByUserId(List movieList) {
		return Optional.of(movieRepo.findAllById(movieList));
	}

	@Override
	public Optional<Movie> findMovieByTitleAndGenre(String movieTitleOrGenre) {
		return movieRepo.findMovieByTitleAndGenre(movieTitleOrGenre);
	}
	
	

}
