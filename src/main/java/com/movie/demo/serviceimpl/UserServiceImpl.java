package com.movie.demo.serviceimpl;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.common.base.Objects;
import com.movie.demo.appconstant.ApplicationConstants;
import com.movie.demo.dto.LoginDto;
import com.movie.demo.dto.LoginResponseDto;
import com.movie.demo.dto.RegisterDto;
import com.movie.demo.dto.ResponseDto;
import com.movie.demo.entity.User;
import com.movie.demo.exception.InvalidUser;
import com.movie.demo.repository.UserRepository;
import com.movie.demo.service.UserService;
import com.movie.demo.utils.CommonUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepo;

	@Override
	public ResponseDto register(RegisterDto registerDto) {

		Optional<User> userData = userRepo.findByEmail(registerDto.getEmailId());

		if (userData.isPresent()) {
			throw new InvalidUser(ApplicationConstants.REGISTER_FAILURE);
		}
		User user = new User();
		user.setEmail(registerDto.getEmailId());
		user.setEname(registerDto.getUserName());
		user.setUserStatus(registerDto.getUserStatus());
		user.setPassword(registerDto.getPassword());
		user.setRegistration_time(CommonUtils.parseDateToString(LocalDateTime.now()));
		userRepo.save(user);
		ResponseDto sucessResponse = new ResponseDto();
		sucessResponse.setMessage("Congrajulations user registered sucessfully");
		sucessResponse.setCode(HttpStatus.CREATED.value());
		return sucessResponse;
	}

	@Override
	public LoginResponseDto validate(LoginDto loginDto) {
		Optional<User> user = userRepo.findByEmail(loginDto.getEmailId());
		LoginResponseDto loginResponseDto = new LoginResponseDto();
		if (user.isPresent()) {
			User temp = user.get();
			loginResponseDto.setCode(HttpStatus.CREATED.value());
			loginResponseDto.setUser_status(temp.getUserStatus());
			if (!Objects.equal(temp.getPassword(), loginDto.getPassword())) {
				loginResponseDto.setMessage("Your email or password is wrong!");
				return loginResponseDto;
			}
			LocalDateTime reg_date = CommonUtils.parseStringToDate(user.get().getRegistration_time());
			if (CommonUtils.findDateDifference(reg_date, LocalDateTime.now())
					&& temp.getUserStatus().equalsIgnoreCase("Guest")) {
				loginResponseDto.setMessage("Your login is expired, please register again!");
				return loginResponseDto;
			}
			loginResponseDto.setMessage("Welcome " + loginDto.getEmailId() + ", You have succuessfully logged in!");
			return loginResponseDto;
		} else {
			loginResponseDto.setCode(HttpStatus.CREATED.value());
			loginResponseDto.setMessage("You have not registered yet, please register!");
			loginResponseDto.setUser_status("Not Registered");
			return loginResponseDto;
		}
	}

	@Override
	public Optional<User> findByUserId(long userId) {
		return userRepo.findById(userId);
	}

}
