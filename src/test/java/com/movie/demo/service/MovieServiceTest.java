package com.movie.demo.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.movie.demo.entity.UserMovie;
import com.movie.demo.repository.MovieRepository;
import com.movie.demo.serviceimpl.MovieServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class MovieServiceTest {

	@InjectMocks
	MovieServiceImpl movieServiceImpl;

	@Mock
	MovieRepository movieRepo;

	@Test
	@DisplayName("Mock the output of the text service using mockito")
	void findMoviesByUserId_WhenFound() {

		UserMovie movie = new UserMovie();
		movie.setMovieId(1L);
		List<UserMovie> movieList = new ArrayList<UserMovie>();
		movieList.add(movie);
		Mockito.when(movieRepo.findMoviesByUserId(1L)).thenReturn(movieList);
		movieServiceImpl.findMoviesByUserId(1L);
		assertEquals(1, movieList.size());
	}

}
