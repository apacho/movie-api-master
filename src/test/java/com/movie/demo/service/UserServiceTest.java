package com.movie.demo.service;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;

import com.movie.demo.dto.RegisterDto;
import com.movie.demo.dto.ResponseDto;
import com.movie.demo.entity.User;
import com.movie.demo.exception.InvalidUser;
import com.movie.demo.repository.UserRepository;
import com.movie.demo.serviceimpl.UserServiceImpl;

@RunWith(PowerMockRunner.class)
public class UserServiceTest {

	@InjectMocks
	UserServiceImpl userServiceImpl;

	@Mock
	UserRepository userRepo;

	@Test
	public void setUp() {

	}

	@Test(expected = InvalidUser.class)
	public void registerNegativeTest1() {

		User user = new User();

		// user.setEmail("Ram@gmail.com");

		RegisterDto dto = new RegisterDto();

		dto.setEmailId("Bhadra@gmail.com");
		dto.setUserName("Bhadra");
		dto.setPassword("Hgbddm@1234");
		dto.setUserStatus("guest");

		Mockito.when(userRepo.findByEmail(Mockito.anyString())).thenReturn(Optional.of(user));

		ResponseDto response = userServiceImpl.register(dto);

		ResponseDto resDto = new ResponseDto();
		resDto.setCode(HttpStatus.CREATED.value());

		assertEquals(HttpStatus.CREATED.value(), resDto.getCode().intValue());

	}

	@Test
	public void registerPositiveTest() {

		User user = new User();

		RegisterDto dto = new RegisterDto();

		dto.setEmailId("Bhadra@gmail.com");
		dto.setUserName("Bhadra");
		dto.setPassword("Hgbddm@1234");
		dto.setUserStatus("guest");

		Mockito.when(userRepo.findByEmail(Mockito.anyString())).thenReturn(Optional.of(user));

		ResponseDto response = userServiceImpl.register(dto);
		ResponseDto resDto = new ResponseDto();
		resDto.setCode(HttpStatus.CREATED.value());

		assertEquals(resDto.getCode().intValue(), response.getCode().intValue());

	}

	@Test(expected = InvalidUser.class)
	public void registerNegativeTest() {

		User user = new User();

		user.setEmail("Ram@gmail.com");

		RegisterDto dto = new RegisterDto();

		dto.setEmailId("Ram@gmail.com");
		dto.setUserName("Ram");
		dto.setPassword("Ram@1234");
		dto.setUserStatus("guest");

		Mockito.when(userRepo.findByEmail(Mockito.anyString())).thenReturn(Optional.of(user));

		ResponseDto response = userServiceImpl.register(dto);

		assertEquals(HttpStatus.CREATED.value(), response.getCode().intValue());

	}

}
